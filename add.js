/* eslint-disable */

function add () {
  this.add('calculate:add', (msg, done) => {
    return done(null, {result: parseFloat(msg.one) + parseFloat(msg.two) });
  });
}

module.exports = add;
