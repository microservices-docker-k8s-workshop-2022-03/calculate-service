/* eslint-disable */
'use strict';

const Seneca = require('seneca');
const add = require('./add');

Seneca()
  .use(add)
  .use('mesh', {
    pin: 'calculate:add'
  })
